import firebase from 'firebase/app'
import 'firebase/database'
import initFirebase from '../../firebase/initFirebase'

initFirebase();


export default async (req, res) => {

    const ref = firebase.database().ref('bookings');
    const { snapshot } = await ref.transaction((bookings) => {
        return bookings
    })

    return res.status(200).json(snapshot.val())
  }