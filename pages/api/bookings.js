import firebase from 'firebase/app'
import 'firebase/database'
import initFirebase from '../../firebase/initFirebase'

initFirebase();

export default (req, res) => {
    try{
        const ref = firebase.database().ref('bookings')

        return ref.once('value', (snapshot) => {
            res.status(200).json(snapshot.val())
        })
    }
    catch(error){
        return res.status(400);
    }
}