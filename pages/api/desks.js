// const desks = [
//     {id:1, booked: false},
//     {id:2, booked: true, bookedBy: 'Martin'},
//     {id:3, booked: false}
// ]
// 
// export default (req, res) => {
//     res.statusCode = 200
//     res.json(desks)
//   }
  

import firebase from 'firebase/app'
import 'firebase/database'
import initFirebase from '../../firebase/initFirebase'

initFirebase();
export default (req, res) => {
    const ref = firebase.database().ref('desks')

    return ref.once('value', (snapshot) => {
        res.status(200).json(snapshot.val())
    })
}