import styles from '../../styles/BookedList.module.css';
import { server } from '../../config';

export async function getServerSideProps() {
        const bookingsReq = await fetch(server + 'api/bookings');
        const bookings = await bookingsReq.json();
        return {
          props: {bookings}
        }
}

export default function BookedList({bookings}) {
    return (
    <div>
        {Object.keys(bookings)?.map((bookingDate) => 
        <>
            <h2>Bookings for {bookingDate}</h2>
            {Object.keys(bookings[bookingDate]).filter(booking => bookings[bookingDate][booking]).map((booking) =>
                <div key={booking}>
                    <span>{`id ${booking}: `}</span>
                    <span>{`${bookings[bookingDate][booking]}`}</span>
                </div>
            )}
        </>
        )}
    </div>
    )
};