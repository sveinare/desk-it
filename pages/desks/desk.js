import PropTypes from "prop-types";
import classnames from "classnames";
import styles from "../../styles/Desks.module.css";

export default function Desk({
  x,
  y,
  width,
  height,
  onClick,
  active,
  booked,
  bookedBy,
  children,
}) {
  //console.log("desk comp", x, y, key);
  //console.log('desk comp style', stl)

  return (
    <div
      className={classnames(
        styles.desk,
        { [styles.active]: active },
        { [styles.booked]: booked }
      )}
      style={{ left: x, top: y, width, height }}
      onClick={() => onClick()}
    >
      {children}
    </div>
  );
}

Desk.propTypes = {
  x: PropTypes.number,
  y: PropTypes.number,
  onClick: PropTypes.func,
  active: PropTypes.bool,
  booked: PropTypes.bool,
  bookedBy: PropTypes.string,
};
