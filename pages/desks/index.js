import { useState } from 'react';
import styles from '../../styles/Desks.module.css';
import classnames from 'classnames';
import { server } from '../../config';
import Desk from './desk';
import BookedList from './bookedList';

export async function getServerSideProps() {
  const desksReq = await fetch(server + 'api/desks');
  const bookingsReq = await fetch(server + 'api/bookings');
  const desks = await desksReq.json();
  const bookings = await bookingsReq.json();
  return {
    props: { desks, bookings },
  };
}

export default function Desks({ desks, bookings }) {
  const [activeDesk, setActiveDesk] = useState();

  const bookDesk = async (e) => {
    e.preventDefault();
    const newBooking = Object.keys(bookings).map((booking) => ({
      ...bookings,
      [booking]: { ...bookings[booking], [activeDesk]: 'Martin' },
    }));
    const res = await fetch(server + 'api/bookDesk', {
      body: JSON.stringify(...newBooking),
      headers: {
        'Content-Type': 'application/json',
      },
      method: 'POST',
    });
    const result = await res.json();
    return result;
  };

  console.log('bookings', bookings);

  return (
    <div>
      <main>
        <div>book your desk now!</div>
        <form onSubmit={(e) => bookDesk(e)}>
          {Object.keys(bookings)?.map((bookingDate) => (
            <div key={bookingDate}>
              <h2>Bookings for {bookingDate}</h2>
              <div className={styles.desksWrapper}>
                <div className={styles.desksBackground}></div>
                <div className={styles.desks}>
                  {Object.keys(desks)?.map((desk) => {
                    const deskObj = desks[desk];
                    //console.log('desk now: ', deskObj)
                    return (
                      <Desk
                        key={desk}
                        x={deskObj.x}
                        y={deskObj.y}
                        width={35}
                        height={20}
                        onClick={() => setActiveDesk(desk)}
                        active={activeDesk === desk}
                        booked={!!bookings[bookingDate][desk]}
                      >
                        {desk}
                      </Desk>
                    );
                  })}
                </div>
              </div>
            </div>
          ))}
          <button type="submit">Book din plass!</button>
        </form>
        <div className={styles.bookedList}>
          <BookedList bookings={bookings} />
        </div>
      </main>
    </div>
  );
}
