import Head from 'next/head';
import styles from '../styles/Home.module.css';
import { server } from '../config';

export async function getServerSideProps() {
  const req = await fetch(server + 'api/hello');
  const name = await req.json();
  return {
    props: name,
  };
}

export default function Home({ name }) {
  return (
    <div className={styles.container}>
      <Head>
        <title>Desk.it</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Welcome to <a href="/desks">desk-it!</a>
        </h1>
        <a href="/desks">Book your desk in a55 now</a>
      </main>

      <footer className={styles.footer}>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          <img src="/vercel.svg" alt="Vercel Logo" className={styles.logo} />
        </a>
      </footer>
    </div>
  );
}
